<?php
/**
 * Template Name: Events Overview
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$templates = array( 'events-overview.twig' );

Timber::render( $templates, $context );