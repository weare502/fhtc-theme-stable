<?php
// check for Timber
if( ! class_exists('Timber') ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
	});
	return;
}

// Required for Alertus integration
use Carbon\Carbon;
require 'vendor/autoload.php';

// Required for Programs and Courses to work
Routes::map('/program/:program/:type', function($params){
	$query = 'post_type=program&name=' . $params['program'];
    Routes::load('program-type.php', $params, $query, 200);
});

// change 'views' directory to 'templates'
Timber::$locations = __DIR__ . '/templates';

class FHTCSite extends TimberSite {

	function __construct() {
		// Theme Support //
		add_theme_support( 'menus' );
		add_theme_support( 'align-wide' );
		add_theme_support( 'post-thumbnails' );

		// Action Hooks //
		add_action( 'after_setup_theme', array( $this, 'after_setup_theme' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'enqueue_block_assets', array( $this, 'backend_frontend_styles' ) );
		add_action( 'admin_head', array( $this, 'admin_head_css' ) );
		add_action( 'admin_menu', array( $this, 'calendar_details_page' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'program_taxonomy' ) );
		add_action( 'init', array( $this, 'foundation_staff_taxonomy' ) );
		add_action( 'acf/init', array( $this, 'render_custom_acf_blocks' ) );

		// Filter Hooks //
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'allowed_block_types', array( $this, 'custom_block_picker' ) );
		add_filter( 'gform_enable_field_label_visibility_settings', array( $this, '__return_true' ) );
		add_filter( 'block_categories', array( $this, 'fhtc_block_category' ), 10, 2 );

		// Column Removal for Comments //
		add_filter( 'manage_edit-page_columns', array( $this, 'disable_admin_columns' ) );

		// Add employee email address to popup form submission
		add_action( 'gform_pre_submission_4',array( $this, 'popup_email_form_pre_submission_handler' ) );

		// For Alertus Integration
		add_action( 'rest_api_init', function () {
			register_rest_route( 'fhtc/v1', '/alertus/', array(
				'methods' => 'POST',
				'callback' => function( $request ){
					// validate
					$secret = get_option('options_alertus_secret_key', false);

					if ( ! isset( $request['secret_key'] ) || $request['secret_key'] !== $secret ){
						return new WP_Error('not_authorized', 'Not Authorized', [
							'status' => 401
						]);
					}

					if ( ! isset( $request['title'] ) || ! isset( $request['body'] ) || ! isset( $request['duration'] ) ){
						return new WP_Error('invalid_parameters', 'Invalid Request Parameters', [
							'status' => 400
						]);
					}

					// set data
					update_option('alertus_data', [
						'title' => esc_html( $request['title'] ),
						'body' => esc_html( $request['body'] ),
						'expiration' => Carbon::now()->addMinutes( intval( $request['duration'] ) )->toDateTimeString(),
					]);

					// response
					$data = ['message' => 'ok'];
					$response = new WP_REST_Response( $data );
					$response->set_status(200);
					return $response;
				}
			));
		});

		add_action('admin_notices', function(){
			if ( get_option('alertus_data') ) {
				$class = 'notice notice-error';
				$message = __( 'Alertus notice is active. Click the button to turn off the alert. It will need to be re-initiated from Alertus if disabled.', 'fhtc' );
				$url = wp_nonce_url(add_query_arg('disable-alertus', true, admin_url()));
				printf( '<div class="%1$s"><p>%2$s</p><p><a href="%3$s" class="button button-primary">Turn OFF Alertus Notice</a></p></div>', esc_attr( $class ), esc_html( $message ), esc_attr( $url ) ); 
			}
		});

		add_action('init', function(){
			if ( isset($_REQUEST['disable-alertus']) && isset($_REQUEST['_wpnonce']) && wp_verify_nonce($_REQUEST['_wpnonce']) ) {
				delete_option('alertus_data');
				wp_redirect(add_query_arg('disable-alertus', true, admin_url()));
			}

			if ( isset($_REQUEST['disable-alertus']) ) {
				add_action('admin_notices', function(){
					$class = 'notice notice-success';
					$message = __( 'Alertus Disabled. If this was a mistake, re-initiate the notice from Alertus.', 'fhtc' );
					printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
				});
			}
		});

		parent::__construct();
	}

	function admin_head_css() {
		?><style type="text/css">
			div.components-notice-list { display: none !important; }
			.acf-fields { border-bottom: 2px solid green !important; }
			.acf-row-handle.order { color: black !important; font-weight: bold !important; font-size: 1.1rem !important; }
			#wp-admin-bar-comments { display: none !important; }
			.update-nag { display: none !important; }
			#menu-posts-employee { margin-top: 11px !important; }
			.acf-editor-wrap .wp-editor-tools { display: none !important; }
			.mce-notification-error { display: none !important; }
		</style><?php
	}

	function enqueue_scripts() {
		wp_enqueue_style( 'fhtc-css', get_stylesheet_directory_uri() . '/style-dist.css', array('extra-css'), '23436491' );
		wp_enqueue_style( 'extra-css', get_stylesheet_directory_uri() . '/extra-styles.css', array(), '23436492' );

		wp_enqueue_script( 'fhtc-js', get_template_directory_uri() . '/static/js/site-dist.js', array( 'jquery', 'jquery-effects-core', 'jquery-effects-slide' ), '23419798' );

		wp_enqueue_script( 'fhtc-slider-js', get_template_directory_uri() . '/static/js/slick.min.js', array( 'jquery' ), '23497998' );

		wp_enqueue_script( 'fhtc-vuejs', get_template_directory_uri() . '/static/js/vue.js', ['underscore'], '2.6.11' );
	}

	// Uses the 'enqueue_block_assets' hook
	function backend_frontend_styles() {
		wp_enqueue_style( 'blocks-css', get_stylesheet_directory_uri() . '/block-style-dist.css' );
	}

	function add_to_context( $context ) {
		$context['site'] = $this;
		$context['date'] = date('F j, Y');
		$context['options'] = get_fields('option');
		$context['is_home'] = is_home();
		$context['csscache'] = filemtime(get_stylesheet_directory() . '/style.css');
		$context['plugin_content'] = TimberHelper::ob_function( 'the_content' );
		$context['home_url'] = home_url('/');
		$context['is_front_page'] = is_front_page();
		$context['get_url'] = $_SERVER['REQUEST_URI'];

		$context['global_alert'] = false;

		// Global Alert Gate
		$alertus = get_option('alertus_data');
		if ( $alertus !== false ){
			$expiration = Carbon::parse($alertus['expiration']);

			if ( Carbon::now()->isBefore($expiration) ){
				$context['global_alert'] = $alertus;
			} else {
				delete_option('alertus_data');
			}
		}

		return $context;
	}

	function after_setup_theme() {
		register_nav_menu( 'primary', 'Main Navigation' );
		register_nav_menu( 'footer-one', 'Footer' );
		register_nav_menu( 'footer-two', 'Footer' );
		register_nav_menu( 'foundation-primary', 'Foundation - Main Navigation' );

		acf_add_options_page( array(
			'page_title' 	=> 'Header / Footer / Calendar Setup',
			'menu_title'	=> 'Global Site Options',
			'menu_slug' 	=> 'fhtc-site-opts',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
	}

	// registers and renders our custom acf blocks
	function render_custom_acf_blocks() {
		require 'acf-block-functions.php';
	}

	// creates a custom category for our theme-specific blocks
	function fhtc_block_category( $categories, $post ) {
		return array_merge( $categories,
		array( array(
				'slug' => 'fhtc-blocks',
				'title' => 'FHTC Blocks'
			),
		));
	}

	// set what blocks are available to the block editor (only for the default template)
	function custom_block_picker( $allowed_blocks ) {
		if( ! current_user_can('manage_options') ) {
			$allowed_blocks = array(
				// Built-in blocks
				'core/image',
				'core/heading',
				'core/paragraph',
				'core/embed',
				'core-embed/facebook',
				'core-embed/youtube',
				'core-embed/twitter',

				// custom acf blocks
				'acf/angled-image-split-right',
				'acf/angled-image-split-left',
				'acf/angled-list-split-left',
				'acf/angled-list-split-right',
				'acf/student-testimonial',
				'acf/foundation-split-cards',
				'acf/foundation-cta-cards',
				'acf/award-grid',
				'acf/dropdown-block',
				'acf/file-grid-block'
			);

			return $allowed_blocks;
		}
	}

	// get rid of clutter
	function disable_admin_columns( $columns ) {
		unset( $columns['comments'] );
		return $columns;
	}

	// add cpts here
	function register_post_types() {
		include_once('custom-post-types/post-type-employee.php');
		//include_once('custom-post-types/post-type-career.php');
		include_once('custom-post-types/post-type-program.php');
		include_once('custom-post-types/post-type-course.php');
		include_once('custom-post-types/post-type-foundation-staff.php');
	}

	// Program taxonomy - for sorting programs and building nav menu items
	function program_taxonomy() {
		$labels = array(
			'name' 				=> _x( 'Program Types', 'nnn' ),
			'singular_name' 	=> _x( 'Program Type', 'nnn' ),
			'search_items' 		=> __( 'Search Program Types', 'nnn' ),
			'all_items' 		=> __( 'All Program Types', 'nnn' ),
			'edit_item' 		=> __( 'Edit Program Type', 'nnn' ),
			'update_item' 		=> __( 'Update Program Type', 'nnn' ),
			'add_new_item' 		=> __( 'Add New Program Type', 'nnn' ),
			'new_item_name' 	=> __( 'New Program Type', 'nnn' ),
			'menu_name' 		=> __( 'Program Types', 'nnn' ),
			'parent_item'		=> NULL, // remove parent items to prevent breaking if someone adds one
		);

		$args = array(
			'hierarchical' 	    => true,
			'labels' 	    	=> $labels,
			'show_ui' 	    	=> true,
			'show_admin_column' => true,
			'query_var'	    	=> true,
			'rewrite'			=> true,
		);
		register_taxonomy( 'program-type', 'program', $args );
	}

	// Foundation Members Taxonomy - for sorting the Foundation Members into groups
	function foundation_staff_taxonomy() {
		$labels = array(
			'name' 				=> _x( 'Member Types', 'nnn' ),
			'singular_name' 	=> _x( 'Member Type', 'nnn' ),
			'search_items' 		=> __( 'Search Member Types', 'nnn' ),
			'all_items' 		=> __( 'All Member Types', 'nnn' ),
			'edit_item' 		=> __( 'Edit Member Type', 'nnn' ),
			'update_item' 		=> __( 'Update Member Type', 'nnn' ),
			'add_new_item' 		=> __( 'Add New Member Type', 'nnn' ),
			'new_item_name' 	=> __( 'New Member Type', 'nnn' ),
			'menu_name' 		=> __( 'Member Types', 'nnn' ),
			'parent_item'		=> NULL, // remove parent items to prevent breaking if someone adds one
		);

		$args = array(
			'hierarchical' 	    => true,
			'labels' 	    	=> $labels,
			'show_ui' 	    	=> true,
			'show_admin_column' => true,
			'query_var'	    	=> true,
			'rewrite'			=> true,
		);
		register_taxonomy( 'member-type', 'foundation-member', $args );
	}

	public function popup_email_form_pre_submission_handler( $form ) {
		// employee email address
		$email = get_field('emp_email', intval(rgpost('input_5')) );
		if ( ! empty($email) ){
			$_POST['input_6'] = $email;
		}
	}
} // End of FHTCSite class

new FHTCSite();

// load the controller file for the Google Calendar API request & setup
// we do this after the site container is created
function load_calendar_data() {
	require 'events-calendar.php';
}

// main site nav
function fhtc_render_primary_menu() {
	wp_nav_menu( array(
		'theme_location' => 'primary',
		'container' => false,
		'menu_id' => 'primary-menu'
	));
}

// footer Menu (Campus Info)
function fhtc_render_footer_menu_one() {
	wp_nav_menu( array(
		'theme_location' => 'footer-one',
		'container' => false,
		'menu_id' => 'footer-menu-one',
		'menu_class' => 'foot-info-menu'
	));
}

// footer Menu (Statements & Disclosures)
function fhtc_render_footer_menu_two() {
	wp_nav_menu( array(
		'theme_location' => 'footer-two',
		'container' => false,
		'menu_id' => 'footer-menu-two',
		'menu_class' => 'foot-info-menu'
	));
}

function fhtc_foundation_primary_menu() {
	wp_nav_menu( array(
		'theme_location' => 'foundation-primary',
		'container' => false,
		'menu_id' => 'primary-menu'
	));
}

// calls get_alert_post() in public root
function get_alert_request() {
	include(ABSPATH . 'get-alerts.php');
}

// run if  _wp_page_template  is not empty (custom template is used)
// for the Default Template it will be empty. (default is used when no template is set)
// Code Courtesy of: Bill Erickson
function ea_disable_editor( $id = false ) {
	if( empty( $id ) ) {
		return false;
	}

	$id = intval( $id );
	$template = get_page_template_slug( $id );

	return !empty( $template );
}

function ea_disable_gutenberg( $can_edit, $post_type ) {
	// additional check to make sure the No Hero Template keeps the block editor
	if( ! ( is_admin() && !empty( $_GET['post'] ) && !get_page_template('no-hero-page.php') ) ) {
		return $can_edit;
	}

	// We want the "no-hero-template" to be GB enabled (only template enabled)
	if( ea_disable_editor( $_GET['post'] ) || ! get_page_template('no-hero-page.php') ) {
		$can_edit = false;
	}

	return $can_edit;
}
add_filter( 'gutenberg_can_edit_post_type', 'ea_disable_gutenberg', 10, 2 );
add_filter( 'use_block_editor_for_post_type', 'ea_disable_gutenberg', 10, 2 );

// ACF now removes all html tags from wysiwyg fields
function add_p_tags_wysiwyg() {
    add_filter('acf_the_content', 'wpautop' );
}
add_action('acf/init', 'add_p_tags_wysiwyg');

// allows the user to select and add employees from the Employee CPT into their respective program(s)
function insert_employee_post_object() {
	// get our array of values (this is a multi-select post-object field)
	$post_objects = get_field('employee_picker');

	if( $post_objects ) : ?>

	<!-- Start desktop styles -->
	<div class="instructor-wrapper">
		<?php foreach( $post_objects as $post_object ) :
			$pid = $post_object->ID; ?>
			<div class="program-instructor-block">
				<?php
					$emp_image_url = get_field('emp_image', $pid)['url'];
					$emp_image_alt = get_field('emp_image', $pid)['alt'];

				if($emp_image_url) : ?>
					<img src="<?= $emp_image_url; ?>" alt="<?= $emp_image_alt; ?>" />
				<?php else : ?>
					<img src="<?= get_stylesheet_directory_uri() . '/static/images/employee-placeholder.jpg' ?>" alt="No photo available" />
				<?php endif; ?>
				<div class="instructor-info">
					<div style="min-height: 7rem;">
						<h5><?= get_the_title($pid); ?></h5>
						<p><?= get_field('emp_title', $pid); ?></p>
						<p class="dept-name"><?= get_field('emp_department', $pid); ?></p>
					</div>

					<?php
						$phone = get_field('emp_phone', $pid);
						$email = get_field('emp_email', $pid);
						$bio = get_field('emp_bio', $pid);
					?>

					<div class="info-dropdown">
						<?php if($phone) : ?>
							<a class="plain-link emp-phone-num" href="tel:<?= $phone; ?>"><?= $phone; ?></a><br/>
						<?php endif; ?>

						<?php if($email) : ?>
							<a id="email-lightbox" data-employee-id="<?= $pid; ?>" class="plain-link" href="#">Send Email</a><br/>
						<?php endif; ?>

						<?php if($bio) : ?>
							<p class="bio"><?= get_field('emp_bio', $pid); ?></p>
						<?php endif;?>
					</div>
					<button class="dropdown-toggle">More Information</button>
				</div>
			</div>
		<?php endforeach; ?>
	</div>

	<!-- Start mobile styles -->
	<div class="mobile-instructor-wrapper">
		<?php foreach( $post_objects as $post_object ) :
			$pid = $post_object->ID;
			$e_dept = get_field('emp_department', $pid);
			?>

			<button class="mobile-dropdown-toggle add-border">
				<span class="h5-heading shorten-title"><?= get_the_title($pid); ?></span><br/>
				<span class="member-title"><?= get_field('emp_title', $pid); ?></span><br/>
				<?php if($e_dept) : ?>
					<span class="dept-name"><?= $e_dept; ?></span>
				<?php endif; ?>
			</button>

			<div class="mobile-instructor-block">
				<img src="<?= get_field('emp_image', $pid)['url']; ?>" alt="<?= get_field('emp_image', $pid)['alt']; ?>" /><br />

				<?php
					$phone = get_field('emp_phone', $pid);
					$email = get_field('emp_email', $pid);
					$bio = get_field('emp_bio', $pid);
				?>

				<?php if($phone) : ?>
					<a class="plain-link emp-phone-num" href="tel:<?= $phone; ?>"><?= $phone; ?></a><br/>
				<?php endif; ?>

				<?php if($email) : ?>
					<a id="email-lightbox" data-employee-id="<?= $pid; ?>" class="plain-link" href="#">Send Email</a><br/>
				<?php endif; ?>

				<?php if($bio) : ?>
					<p class="bio"><?= get_field('emp_bio', $pid); ?></p>
				<?php endif;?>
			</div> <!-- /.mobile-instructor-block -->
		<?php endforeach; ?>
	</div> <!-- /.mobile-instructor-wrapper -->
	<?php endif; // both mobile & desktop styles are within this check
}