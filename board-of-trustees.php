<?php
/**
 * Template Name: Board of Trustees
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$templates = array( 'board-of-trustees.twig' );
Timber::render( $templates, $context );