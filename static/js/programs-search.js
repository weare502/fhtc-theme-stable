(function($) {
	$(document).ready(function() {

		var filterItems = function(){
			var program = $('#category-select-field').val();
			var degreeType = $('#program-formats').val();
			// console.log(program, degreeType);

			$('.program-card').each(function(){
				$(this).show();
				var show = true;

				if ( degreeType !== '' && $(this).attr('data-' + degreeType) !== 'true' ){
					show = false;
				}

				if ( show && program !== 'All Programs' && !$(this).hasClass(program) ){
					show = false;
				}

				if ( ! show ){
					$(this).hide();
				}
			});
		};

		$('#all-programs-filter-reset').click(function(){
			$('#category-select-field').val('All Programs').trigger('change');
			$('#program-formats').val('').trigger('change');
		});

		$('#program-formats').change(filterItems);
		$('#category-select-field').change(filterItems);

		// reset on page load
		$('#all-programs-filter-reset').click();

	}); // End document.ready
})(jQuery);