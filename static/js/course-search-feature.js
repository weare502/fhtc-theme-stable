/**
 * Controls the sorting and search feature on the Course Schedules Page.
*/

function onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
}

Vue.component('course-schedules', {
	data: function(){
		return {
			courses: [],
			courseFormats: [],
			terms: [],
			filters: {
				keyword: '',
				term: '',
				courseFormat: '',
			}
		}
	},
	mounted: function(){
		window.globalCourses.map(i => {
			i.toggleOpen = false;
			this.courses.push(i);
		});

		this.courses = this.courses.map(function(course){
			course.sections = course.custom.course_info;

			course.sections = course.sections.map(function(section){
				section.term = {
					start: section.AC_Term_Begin_Date ? section.AC_Term_Begin_Date : section.AC_Term_Desc + ' ' + section.AC_Year_Desc,
					title: section.AC_Term_Desc + ' ' + section.AC_Year_Desc
				};

				if ( section.ARC === null ) {
					section.ARC = section.CrsDiv + section.CrsNum;
				}

				section.seatsLeft = section.CrsCapacity - section.CrsEnrolled;
				section.full = section.CrsCapacity === section.CrsEnrolled;

				section.courseFormat = 'On Campus';
				if ( section.GenEdCourse == 'Y' ){
					section.courseFormat = 'General Education'
				}

				if ( section.ConEdCourse == 'Y' ){
					section.courseFormat = 'Continuing Education'
				}

				if ( section.EveningCourse == 'Y' ){
					section.courseFormat = 'Evening'
				}

				if ( section.WeekendCourse == 'Y' ){
					section.courseFormat = 'Weekend'
				}

				if ( section.HybridCourse == 'Y' ){
					section.courseFormat = 'Hybrid'
				}

				if ( section.OnlineCourse == 'Y' ){
					section.courseFormat = 'Online'
				}

				return section;
			});

			return course;
		});

		this.terms = _.flatten(this.courses.map(function(course){
			// console.log(course);
			return course.sections.map(function(section){
				return section.term;
			});
		})).filter( (obj, pos, arr) => {
				return arr.map(mapObj => mapObj['start']).indexOf(obj['start']) === pos;
			}).sort( (a, b) => (a.start > b.start) ? 1 : -1);

		this.courseFormats = _.flatten(this.courses.map(function(course){
			return course.sections.map(function(section){
				return section.courseFormat;
			});
		})).filter(onlyUnique);
	},
	methods: {
		matchesTerm: function(section){
			if (this.filters.term === ''){
				return true;
			}
			return section.term.title === this.filters.term;
		},
		matchesFormat: function(section){
			if (this.filters.courseFormat === ''){
				return true;
			}
			return section.courseFormat === this.filters.courseFormat;
		},
		matchesKeyword: function(section){
			if (this.filters.keyword === ''){
				return true;
			}
			return JSON.stringify(section).toLowerCase().includes(this.filters.keyword.toLowerCase());
		},
		resetFilters: function(){
			this.filters = {
				term: '',
				courseFormat: '',
				keyword: '',
			}
		},
		getSchedules: function(section){
			var schedules = [];
			section.Schedules.map(function(schedule){
				schedules.push(schedule.Schedule)
			});
			return schedules;
		},
		formatDate: function(date){
			var parsed = window.moment(date, 'MM/DD/YYYY');
			return parsed.format('MM/DD/YY');
		},
		formatTime: function(time){
			var parsed = window.moment(time, 'hh:mm:ss A');
			//'g:i A'
			return parsed.format('h:mm A');
		},
		toggleOpenCourse: function(course){
			let c = this.courses.find(c => c.ID === course.ID)
			let i = this.courses.indexOf(c);

			let toggleState = !course.toggleOpen;
			if ( i !== -1 ){
				this.courses[i].toggleOpen = toggleState;
			}
		}
	},
	computed: {
		filteredCourses: function(){
			var courses = [];
			
			this.courses.map((course) => {
				var newCourse = JSON.parse(JSON.stringify(course));
				var courseSections = newCourse.sections.filter((section) => {
					if ( this.matchesTerm(section) && this.matchesFormat(section) && this.matchesKeyword(section) ){
						return true;
					} else {
						return false;
					}
				});

				if ( courseSections.length > 0 ){
					newCourse.sections = courseSections;
					courses.push(newCourse);
				}
				
			});

			return courses;
		}
	}
});

new Vue({
	el: '#courses',
	delimiters: ['${', '}']
});
