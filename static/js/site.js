/* eslint no-inner-declarations: 0 */
(function($) {
    $(document).ready(function() {

		// mobile menu functions
		$(function siteNavigation() {
			$('#menu-toggle').click(function() {
				$('#primary-menu').toggleClass('menu-open');
				$('.x-bar').toggleClass('x-bar-active');
			});

			$('.sub-menu').append('<li><button id="mobile-back-btn" class="fa fa-long-arrow-left menu-back"><span class="sr-only">Back</span></button></li>');

			$('.sub-menu').on('click', 'button.menu-back', function(e){
                var $menu = $(this).parents('.sub-menu:first');
				$menu.removeClass('sub-menu-open');

                // stop the event from bubbling back up and clicking the top menu item
                e.stopPropagation();
            });

            $('.menu-item-has-children').click(function(e){
                var $sub = $(this).find('.sub-menu:first');
                if ( ! $sub.hasClass('sub-menu-open') && window.innerWidth < 1024 ){
                    // don't follow links on first click
                    e.preventDefault();
                }

                $sub.addClass('sub-menu-open');
                e.stopPropagation();
            });
		});

		$(function closeAlert() {
			var $container = $('.alert-container');
			
			if(docCookies.keys().indexOf($container.data('alert-id')) !== -1 ){
				$container.hide();
			}

			$('.alert-banner__close').click(function() {
				var expires = new Date();
				// expire cookie in 30 days
				expires.setDate(expires.getDate() + 30);
				docCookies.setItem( $container.data('alert-id'), true, expires);
				$container.hide();
			});
		});

		$(function videoModalController() {
			// Video modal overlay - centered and locked
			$('.home-video-button').click(function() {
				$('.video-modal').fadeToggle();
				$('body').css('position', 'fixed');
			});

			// Video Modal Close button
			$('.home-video-close').click(function() {
				$('.video-modal').fadeToggle();
				$('body').css('position', 'unset');
				$('#ytplayer').attr('src', $('#ytplayer').attr('src'));
			});

			// Close modal when body is clicked as well
			$(document).click(function(e) {
				// close unless the modal or the play button are clicked
				if( ! $(e.target).closest('video-modal, .home-video-button').length ) {
					$('.video-modal').fadeOut();
					$('body').css('position', 'unset');
					$('#ytplayer').attr('src', $('#ytplayer').attr('src'));
				}
			});
		});

		if( $('body').hasClass('page-template-front-page') && $('body').width() > 480 ) {
			$(function homePageLoader() {

				// setup vars and functions
				var $pbar1, $pbar2, $pbar3, $slick, tick, percentTime;
				var time = 9;
				$slick = $('.slider');

				// vars for progress bar and animating slide overlays
				$pbar1 = $('.progress-bar-handle .progress-bar-1');
				$pbar2 = $('.progress-bar-handle .progress-bar-2');
				$pbar3 = $('.progress-bar-handle .progress-bar-3');
				var $blue_overlay = $('.home-blue-trapezoid-overlay');
				var $text_overlay = $('.home-header-overlay-text');

				// Slick slider setup
				$slick.slick({
					slidesToShow: 1,
					slidesToScroll: 1,
					pauseOnHover: false,
					pauseOnFocus: false,
					arrows: false,
					dots: false,
					draggable: false,
				});

				// Slide order will be 0 2 1   (moves in reverse order but starts at position 1)
				function startProgressbar() {
					resetProgressbar();
					percentTime = 0;
					tick = setInterval(interval, 10);
				}

				function interval() {
					percentTime += 1 / (time + 0.1);

					if($slick.slick('slickCurrentSlide') === 0) {
						$pbar1.css({
							width: percentTime + "%"
						});

						// when the first bar-fill is called, reset the other bars
						$pbar2.css({
							width: 0
						});
						$pbar3.css({
							width: 0
						});
					} else if($slick.slick('slickCurrentSlide') === 2) {
						$pbar2.css({
							width: percentTime + "%"
						});
					} else if($slick.slick('slickCurrentSlide') === 1) {
						$pbar3.css({
							width: percentTime + "%"
						});
					}

					if(percentTime >= 5 && percentTime <= 10) {
						$text_overlay.fadeIn(800);
						$blue_overlay.show('slide', { direction: 'left' }, 400);
					}

					if(percentTime >= 92 && percentTime < 100) {
						$text_overlay.fadeOut(400);
						$blue_overlay.hide('slide', { direction: 'left' }, 800);
					}

					if(percentTime >= 100) {
						$slick.slick('slickPrev');
						startProgressbar();
					}
				}

				function resetProgressbar() {
					if($slick.slick('slickCurrentSlide') === 0) {
						$pbar1.css({
							width: 0 + '%'
						});
					} else if($slick.slick('slickCurrentSlide') === 2) {
						$pbar2.css({
							width: 0 + '%'
						});
					} else if($slick.slick('slickCurrentSlide') === 1) {
						$pbar3.css({
							width: 0 + '%'
						});
					}
					clearTimeout(tick);
				}

				// once slider is setup, start progress bars
				startProgressbar();

				// Gets and Sorts the option values when the "Go" button is clicked
				$(function startAdventure() {
					$('#go-button').click(function() {
						// Left box (I'm a...)
						var box_one_option = $('#picker-one').find(':selected').val();

						var box_one_val1 = $('.op1-val-1').val();
						var box_one_val2 = $('.op1-val-2').val();
						var box_one_val3 = $('.op1-val-3').val();
						var box_one_val4 = $('.op1-val-4').val();
						var box_one_val5 = $('.op1-val-5').val();

						var return1_one = $('.ret1-val-1');
						var return1_two = $('.ret1-val-2');
						var return1_three = $('.ret1-val-3');
						var return1_four = $('.ret1-val-4');
						var return1_five = $('.ret1-val-5');

						if(box_one_option === box_one_val1) {
							return1_two.fadeOut(0);
							return1_three.fadeOut(0);
							return1_four.fadeOut(0);
							return1_five.fadeOut(0);
							return1_one.fadeIn(850); // fade in
						} else if(box_one_option === box_one_val2) {
							return1_one.fadeOut(0);
							return1_three.fadeOut(0);
							return1_four.fadeOut(0);
							return1_five.fadeOut(0);
							return1_two.fadeIn(850); // fade in
						} else if(box_one_option === box_one_val3) {
							return1_one.fadeOut(0);
							return1_two.fadeOut(0);
							return1_four.fadeOut(0);
							return1_five.fadeOut(0);
							return1_three.fadeIn(850); // fade in
						} else if(box_one_option === box_one_val4) {
							return1_one.fadeOut(0);
							return1_two.fadeOut(0);
							return1_three.fadeOut(0);
							return1_five.fadeOut(0);
							return1_four.fadeIn(850); // fade in
						} else if(box_one_option === box_one_val5) {
							return1_one.fadeOut(0);
							return1_two.fadeOut(0);
							return1_three.fadeOut(0);
							return1_four.fadeOut(0);
							return1_five.fadeIn(850); // fade in
						}

						// Middle box (I'm interested in...)
						var box_two_option = $('#picker-two').find(':selected').val();

						var box_two_val1 = $('.op2-val-1').val();
						var box_two_val2 = $('.op2-val-2').val();
						var box_two_val3 = $('.op2-val-3').val();
						var box_two_val4 = $('.op2-val-4').val();

						var return2_one = $('.ret2-val-1');
						var return2_two = $('.ret2-val-2');
						var return2_three = $('.ret2-val-3');
						var return2_four = $('.ret2-val-4');

						if(box_two_option === box_two_val1) {
							return2_two.fadeOut(0);
							return2_three.fadeOut(0);
							return2_four.fadeOut(0);
							return2_one.fadeIn(850); // fade in
						} else if(box_two_option === box_two_val2) {
							return2_one.fadeOut(0);
							return2_three.fadeOut(0);
							return2_four.fadeOut(0);
							return2_two.fadeIn(850); // fade in
						} else if(box_two_option === box_two_val3) {
							return2_one.fadeOut(0);
							return2_two.fadeOut(0);
							return2_four.fadeOut(0);
							return2_three.fadeIn(850); // fade in
						} else if(box_two_option === box_two_val4) {
							return2_one.fadeOut(0);
							return2_two.fadeOut(0);
							return2_three.fadeOut(0);
							return2_four.fadeIn(850); // fade in
						}

						// Right box (I want to...)
						var box_three_option = $('#picker-three').find(':selected').val();

						var box_three_val1 = $('.op3-val-1').val();
						var box_three_val2 = $('.op3-val-2').val();
						var box_three_val3 = $('.op3-val-3').val();
						var box_three_val4 = $('.op3-val-4').val();

						var return3_one = $('.ret3-val-1');
						var return3_two = $('.ret3-val-2');
						var return3_three = $('.ret3-val-3');
						var return3_four = $('.ret3-val-4');

						if(box_three_option === box_three_val1) {
							return3_two.fadeOut(0);
							return3_three.fadeOut(0);
							return3_four.fadeOut(0);
							return3_one.fadeIn(850); // fade in
						} else if(box_three_option === box_three_val2) {
							return3_one.fadeOut(0);
							return3_three.fadeOut(0);
							return3_four.fadeOut(0);
							return3_two.fadeIn(850); // fade in
						} else if(box_three_option === box_three_val3) {
							return3_one.fadeOut(0);
							return3_two.fadeOut(0);
							return3_four.fadeOut(0);
							return3_three.fadeIn(850); // fade in
						} else if(box_three_option === box_three_val4) {
							return3_one.fadeOut(0);
							return3_two.fadeOut(0);
							return3_three.fadeOut(0);
							return3_four.fadeIn(850); // fade in
						}
					}); // end of "go" button click function
				}); // end of 'Start Your Future' logic and sorting
			}); // end of homepage load function
		} // end of body class check (if)

		// Student Testimonial Video (hide blue overlay on Play)
		$('#testimonial-video').on('play', function() {
			$('#testimonial-overlay').css('z-index', '0');
		});

		$('#testimonial-video').on('ended', function() {
			$('#testimonial-overlay').css('z-index', '2');
		});

		// Program Instructors 'More Info' box calcs (div)
		$('.info-dropdown').each(function() {
			// Do not use height() - breaks when searching
			$(this).hide();
		});

		// Program Instructors dropdown box (button)
		$('.dropdown-toggle').click(function() {
			var $this = $(this);
			var $height = $this.height();
			$this.text( $this.text() == 'More Information' ? 'Less' : 'More Information' );
			$this.toggleClass('dropdown-toggled');
			$this.prev('.info-dropdown').slideToggle(700);
			$this.show(); // needed when searching
		});

		// Program Instructors Mobile dropdown box (div)
		$('.mobile-instructor-block').each(function() {
			// height does not work here (gets wrong height)
			$(this).hide();
		});

		// Instructors Mobile dropdown (button)
		$('.mobile-dropdown-toggle').click(function() {
			$(this).toggleClass('mobile-dropdown-toggled');
			$(this).next('.mobile-instructor-block').slideToggle(700);
			$(this).toggleClass('add-border');
		});

		// BLOCK - dropdown box
		$('.dropdown-content').each(function() {
			$(this).hide();
		});

		$('.dropdown-block-title').click(function() {
			// fires on first click (when content is expanded)
			if( $(this).hasClass('set-border')){
				$(this).removeClass('set-border');
				$(this).next('.dropdown-content').slideToggle(700);
				$(this).attr('aria-pressed', 'true');
			} else {
				// fires on second click (when content is closed)
				$(this).next('.dropdown-content:first').slideToggle(700, function(){
					$(this).prev('.dropdown-block-title').addClass('set-border');
					$(this).prev('.dropdown-block-title').attr('aria-pressed', 'false');
				});
			}
			// always fire
			$(this).toggleClass('chevron-rotate');
		});
		// end of dropdown block

		$('#courses').click('.course-accordion-toggle', function(e){
			var $button = $(e.target).closest('.course-item').find('.course-accordion-toggle');
			var $course = $button.parent('.course-item');
			var atts = '[data-year="' + $course.attr('data-year') + '"][data-term="'  + $course.attr('data-term') + '"][data-arc="'  + $course.attr('data-arc') + '"]';
			var $courses = $('.course-item' + atts);

			$courses.toggleClass('visible');

			// some "visible" logic in course-search-feature.js to reset all visible when filters are applied
		});


		// only load this code on employees page - breaks js everywhere else
		if( $('body').hasClass('page-template-employee-directory') ) {
			// reset on page load.
			$('#employee-search').val('');

			$('#employee-search').fastLiveFilter('.employee-wrapper', {
				callback: function(){
					var $el = $('#employee-search');
					if($el.val().length > 0) {
						$('body').addClass('searching-employees');
					} else {
						$('body').removeClass('searching-employees');
					}
					if ($('#employee-departments').val() == ''){
						return;
					}
					$('.employee-wrapper:not([data-department="' + $('#employee-departments').val() + '"])').hide();
				}
			});

			$('#employee-departments').on('change', function(){
				if ($(this).val() == ''){
					$('.employee-wrapper').show();
					$('#employee-search').trigger('change');
					return;
				}

				$('.employee-wrapper').show();
				$('.employee-wrapper:not([data-department="' + $(this).val() + '"])').hide();
				$('#employee-search').trigger('change');
			});

			$('button.employee-directory-reset').click(function(){
				$('#employee-departments').val('').trigger('change');
				$('#employee-search').val('').trigger('change');
			});
		} // End of employee special search

		// Mobile search bar toggler
		$('.toggle-search').click(function(){
			$(this).parent().toggleClass('open');
		});

		// make dropdowns behave on interior templates
		$('.dropdown-content div h1').find('span').css('font-weight', 'bold');
		$('.dropdown-content div h2').find('span').css('font-weight', 'bold');
		$('.dropdown-content div h3').find('span').css('font-weight', 'bold');

		$('.dropdown-content div h4').find('span').css({
			'font-weight': 'bold',
			'font-size': '1rem'
		});

		$('.dropdown-content div h5').find('span').css('font-weight', 'bold');
		$('.dropdown-content div h6').find('span').css('font-weight', 'bold');

		// Popup maker integration
		$('body').on('click', '.pum-trigger', function(event){
			window.mostRecentEmployeeID = $(this).data('employee-id');
		});

		jQuery(document).on('pumBeforeOpen', function(event){
			// must be form ID 4, field ID 5
			var $hiddenField = $('#input_4_5');
			// Add Employee's ID to hidden form field
			$hiddenField.attr('value', window.mostRecentEmployeeID).trigger('change'); // specific to form w/ ID 4, field 5
		});

	}); // end Document.Ready
})(jQuery);

jQuery.fn.fastLiveFilter = function(list, options) {
	// Options: input, list, timeout, callback
	options = options || {};
	list = jQuery(list);
	var input = this;
	var lastFilter = '';
	var timeout = options.timeout || 0;
	var callback = options.callback || function() {};
	
	var keyTimeout;
	
	// NOTE: because we cache lis & len here, users would need to re-init the plugin
	// if they modify the list in the DOM later.  This doesn't give us that much speed
	// boost, so perhaps it's not worth putting it here.
	var lis = list.children();
	var len = lis.length;
	var oldDisplay = len > 0 ? lis[0].style.display : "block";
	callback(len); // do a one-time callback on initialization to make sure everything's in sync
	
	input.change(function() {
		// var startTime = new Date().getTime();
		var filter = input.val().toLowerCase();
		var li, innerText;
		var numShown = 0;
		for (var i = 0; i < len; i++) {
			li = lis[i];
			innerText = !options.selector ? 
				(li.textContent || li.innerText || "") : 
				jQuery(li).find(options.selector).text();
			
			if (innerText.toLowerCase().indexOf(filter) >= 0) {
				if (li.style.display == "none") {
					li.style.display = oldDisplay;
				}
				numShown++;
			} else {
				if (li.style.display != "none") {
					li.style.display = "none";
				}
			}
		}
		callback(numShown);
		// var endTime = new Date().getTime();
		// console.log('Search for ' + filter + ' took: ' + (endTime - startTime) + ' (' + numShown + ' results)');
		return false;
	}).keydown(function() {
		clearTimeout(keyTimeout);
		keyTimeout = setTimeout(function() {
			if( input.val() === lastFilter ) return;
			lastFilter = input.val();
			input.change();
		}, timeout);
	});
	return this; // maintain jQuery chainability
};

/*\
|*|
|*|  :: cookies.js ::
|*|
|*|  A complete cookies reader/writer framework with full unicode support.
|*|
|*|  Revision #3 - July 13th, 2017
|*|
|*|  https://developer.mozilla.org/en-US/docs/Web/API/document.cookie
|*|  https://developer.mozilla.org/User:fusionchess
|*|  https://github.com/madmurphy/cookies.js
|*|
|*|  This framework is released under the GNU Public License, version 3 or later.
|*|  http://www.gnu.org/licenses/gpl-3.0-standalone.html
|*|
|*|  Syntaxes:
|*|
|*|  * docCookies.setItem(name, value[, end[, path[, domain[, secure]]]])
|*|  * docCookies.getItem(name)
|*|  * docCookies.removeItem(name[, path[, domain]])
|*|  * docCookies.hasItem(name)
|*|  * docCookies.keys()
|*|
\*/

var docCookies = {
	getItem: function (sKey) {
		if (!sKey) { return null; }
		return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
	},
	setItem: function (sKey, sValue, vEnd, sPath, sDomain, bSecure) {
		if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) { return false; }
		var sExpires = "";
		if (vEnd) {
			switch (vEnd.constructor) {
				case Number:
				sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + vEnd;
				/*
				Note: Despite officially defined in RFC 6265, the use of `max-age` is not compatible with any
				version of Internet Explorer, Edge and some mobile browsers. Therefore passing a number to
				the end parameter might not work as expected. A possible solution might be to convert the the
				relative time to an absolute time. For instance, replacing the previous line with:
				*/
				/*
				sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; expires=" + (new Date(vEnd * 1e3 + Date.now())).toUTCString();
				*/
				break;
			case String:
				sExpires = "; expires=" + vEnd;
				break;
			case Date:
			sExpires = "; expires=" + vEnd.toUTCString();
			break;
		}
	}
		document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
		return true;
	},
	removeItem: function (sKey, sPath, sDomain) {
		if (!this.hasItem(sKey)) { return false; }
		document.cookie = encodeURIComponent(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "");
		return true;
	},
	hasItem: function (sKey) {
		if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) { return false; }
		return (new RegExp("(?:^|;\\s*)" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
	},
	keys: function () {
		var aKeys = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
		for (var nLen = aKeys.length, nIdx = 0; nIdx < nLen; nIdx++) { aKeys[nIdx] = decodeURIComponent(aKeys[nIdx]); }
		return aKeys;
	}
};