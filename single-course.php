<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$sections = $post->meta('course_info');
$terms = [];

foreach ( $sections as $section ){
    $year = $section['AC_Year'];
    switch ( trim($section['AC_Term']) ) {
        case 'SU':
            $term = 'Summer';
            break;
        case 'SP':
            $term = 'Spring';
            break;
        case 'FA':
            $term = 'Fall';
            break;
        default:
            $term = '';
            break;
    }
    $full_term = $year . ' ' . $term;
    
    if ( isset( $terms[ $full_term ] ) ){
        $terms[$full_term][] = $section;
    } else {
        $terms[$full_term] = [$section];
    }
}

$context['terms'] = $terms;

// echo '<pre>';
// var_dump($terms); die();
Timber::render( array( 'single-course.twig' ), $context );