<?php
// This functions file is for all custom blocks added via ACF
// Reference: https://www.advancedcustomfields.com/resources/acf_register_block/

// Table of Contents (use cmd+f to search for the block names inside the brackets)
/**
 *  - 3 Column Grid : [three-col-grid]
 *  - Angled Image Split : [angled-split]
*/

if( function_exists('acf_register_block_type') ) :
	include 'acf-blocks-callback.php'; // pass-off to let Timber render the blocks

	/** Blocks **/
	// Info left and image right, both with inner angles
	$angled_split_right = array(
		'name' => 'angled-image-split-right',
		'title' => __( 'Angled Image Split Right', 'fhtc' ),
		'description' => __( 'Creates an info box and image with the inner edges slanted towards the middle (Image Right).', 'fhtc' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'fhtc-blocks',
		'align' => 'wide',
		'icon' => 'editor-indent',
		'mode' => 'auto',
		'supports' => array( 'mode' => true ),
		'keywords' => array( 'angled', 'angle', 'slanted', 'slant', 'image', 'split', 'right' )
	);
	acf_register_block_type( $angled_split_right );

	// Info Right and image left, both with inner angles
	$angled_split_left = array(
		'name' => 'angled-image-split-left',
		'title' => __( 'Angled Image Split Left', 'fhtc' ),
		'description' => __( 'Creates an info box and image with the inner edges slanted towards the middle (Image Left).', 'fhtc' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'fhtc-blocks',
		'align' => 'wide',
		'icon' => 'editor-outdent',
		'mode' => 'auto',
		'supports' => array( 'mode' => true ),
		'keywords' => array( 'angled', 'angle', 'slanted', 'slant', 'image', 'split', 'left' )
	);
	acf_register_block_type( $angled_split_left );

	// List-Info Right and image left, both with inner angles
	$angled_list_left = array(
		'name' => 'angled-list-split-left',
		'title' => __( 'Angled List Split Left', 'fhtc' ),
		'description' => __( 'Creates an info box and image with the inner edges slanted towards the middle (Image Left).', 'fhtc' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'fhtc-blocks',
		'align' => 'wide',
		'icon' => 'editor-outdent',
		'mode' => 'auto',
		'supports' => array( 'mode' => true ),
		'keywords' => array( 'angled', 'angle', 'slanted', 'list', 'image', 'split', 'left' )
	);
	acf_register_block_type( $angled_list_left );

	// List-Info Right and image left, both with inner angles
	$angled_list_right = array(
		'name' => 'angled-list-split-right',
		'title' => __( 'Angled List Split Right', 'fhtc' ),
		'description' => __( 'Creates an info box and image with the inner edges slanted towards the middle (Image Left).', 'fhtc' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'fhtc-blocks',
		'align' => 'wide',
		'icon' => 'editor-indent',
		'mode' => 'auto',
		'supports' => array( 'mode' => true ),
		'keywords' => array( 'angled', 'angle', 'slanted', 'list', 'image', 'split', 'right' )
	);
	acf_register_block_type( $angled_list_right );

	$stu_testimonial = array(
		'name' => 'student-testimonial',
		'title' => __( 'Student Testimonial', 'fhtc' ),
		'description' => __( 'Creates an info box and image with the inner edges slanted towards the middle.', 'fhtc' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'fhtc-blocks',
		'align' => 'wide',
		'icon' => 'format-chat',
		'mode' => 'auto',
		'supports' => array( 'mode' => true ),
		'keywords' => array( 'angled', 'angle', 'slanted', 'student', 'image', 'split', 'testimonial' )
	);
	acf_register_block_type( $stu_testimonial );

	$fnd_split_cards = array(
		'name' => 'foundation-split-cards',
		'title' => __( 'Foundation Split Cards', 'fhtc' ),
		'description' => __( 'Creates an image and blue overlay. Image is left.', 'fhtc' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'fhtc-blocks',
		'align' => 'wide',
		'icon' => 'index-card',
		'mode' => 'auto',
		'supports' => array( 'mode' => true ),
		'keywords' => array( 'split', 'card', 'image', 'overlay', 'left' )
	);
	acf_register_block_type( $fnd_split_cards );

	$fnd_cta_cards = array(
		'name' => 'foundation-cta-cards',
		'title' => __( 'Call To Action Cards', 'fhtc' ),
		'description' => __( 'Creates a row of up-to 3 CTA Cards. (Center aligned if less than 3)', 'fhtc' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'fhtc-blocks',
		'align' => 'wide',
		'icon' => 'index-card',
		'mode' => 'auto',
		'supports' => array( 'mode' => true ),
		'keywords' => array( 'call', 'to', 'action', 'card', 'image', 'overlay' )
	);
	acf_register_block_type( $fnd_cta_cards );

	$award_grid = array(
		'name' => 'award-grid',
		'title' => __( 'Awards Grid', 'fhtc' ),
		'description' => __( 'Creates a 3 column, endless row grid.', 'fhtc' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'fhtc-blocks',
		'align' => 'wide',
		'icon' => 'grid-view',
		'mode' => 'auto',
		'supports' => array( 'mode' => true ),
		'keywords' => array( 'grid', 'card', 'image', 'overlay', 'left' )
	);
	acf_register_block_type( $award_grid );

	$dropdown_block = array(
		'name' => 'dropdown-block',
		'title' => __( 'Dropdown Block', 'fhtc' ),
		'description' => __( 'The block title is clicked which will open the dropdown. The content will be folded into the title section', 'fhtc' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'fhtc-blocks',
		'align' => 'wide',
		'icon' => 'image-flip-vertical',
		'mode' => 'auto',
		'supports' => array( 'mode' => true ),
		'keywords' => array( 'drop', 'down', 'block', 'expand' )
	);
	acf_register_block_type( $dropdown_block );

	$file_grid_block = array(
		'name' => 'file-grid-block',
		'title' => __( 'File Grid Block', 'fhtc' ),
		'description' => __( 'Creates a 3 column, endless grid with either links, pdfs, or both.', 'fhtc' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'fhtc-blocks',
		'align' => 'wide',
		'icon' => 'grid-view',
		'mode' => 'auto',
		'supports' => array( 'mode' => true ),
		'keywords' => array( 'file', 'grid', '3', 'column', 'upload', 'pdf' )
	);
	acf_register_block_type( $file_grid_block );

endif;