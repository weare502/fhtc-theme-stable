<?php
/**
 * Template Name: Foundation About
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['member_category'] = Timber::get_terms([ 'taxonomies' => 'member-type' ]);

$context['foundation_members'] = Timber::get_posts([
	'post_type' => 'foundation-member',
	'posts_per_page' => -1,
	'orderby' => 'date',
	'order' => 'ASC'
]);

$templates = array( 'foundation/foundation-about.twig' );

Timber::render( $templates, $context );