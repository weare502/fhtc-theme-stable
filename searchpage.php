<?php
/**
 * Template Name: Search Results Page
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

//$context['search_form'] = get_search_form();
$templates = array( 'searchpage.twig' );

Timber::render( $templates, $context );