<?php

$labels = array(
	'name'               => __( 'Foundation Members', 'fhtc' ),
	'singular_name'      => __( 'Foundation Member', 'fhtc' ),
	'add_new'            => _x( 'Add Foundation Member', 'fhtc', 'fhtc' ),
	'add_new_item'       => __( 'Add Foundation Member', 'fhtc' ),
	'edit_item'          => __( 'Edit Foundation Member', 'fhtc' ),
	'new_item'           => __( 'New Foundation Member', 'fhtc' ),
	'view_item'          => __( 'View Foundation Member', 'fhtc' ),
	'search_items'       => __( 'Search Foundation Members', 'fhtc' ),
	'not_found'          => __( 'No Foundation Members found', 'fhtc' ),
	'not_found_in_trash' => __( 'No Foundation Members found in Trash', 'fhtc' ),
	'parent_item_colon'  => __( 'Parent Foundation Member:', 'fhtc' ),
	'menu_name'          => __( 'Foundation Members', 'fhtc' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array('member-type'),
	'public'              => false, // we are calling this on foundation about page
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'show_in_rest'		  => true,
	'menu_position'       => 30,
	'menu_icon'           => 'dashicons-groups',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => true,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => false,
	'capability_type'     => 'post',
	'supports'            => array( 'title' ),
);
register_post_type( 'foundation-member', $args );