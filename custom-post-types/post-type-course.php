<?php

$labels = array(
	'name'               => __( 'Courses', 'fhtc' ),
	'singular_name'      => __( 'Course', 'fhtc' ),
	'add_new'            => _x( 'Add Course', 'fhtc', 'fhtc' ),
	'add_new_item'       => __( 'Add Course', 'fhtc' ),
	'edit_item'          => __( 'Edit Course', 'fhtc' ),
	'new_item'           => __( 'New Course', 'fhtc' ),
	'view_item'          => __( 'View Course', 'fhtc' ),
	'search_items'       => __( 'Search Courses', 'fhtc' ),
	'not_found'          => __( 'No Courses found', 'fhtc' ),
	'not_found_in_trash' => __( 'No Courses found in Trash', 'fhtc' ),
	'parent_item_colon'  => __( 'Parent Course:', 'fhtc' ),
	'menu_name'          => __( 'Courses', 'fhtc' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'show_in_rest'		  => true,
	'menu_icon'           => 'dashicons-book-alt',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true, // main Course listing/group page
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array( 'title' ),
);
register_post_type( 'course', $args );