<?php

$labels = array(
	'name'               => __( 'Careers', 'fhtc' ),
	'singular_name'      => __( 'Career', 'fhtc' ),
	'add_new'            => _x( 'Add Career', 'fhtc', 'fhtc' ),
	'add_new_item'       => __( 'Add Career', 'fhtc' ),
	'edit_item'          => __( 'Edit Career', 'fhtc' ),
	'new_item'           => __( 'New Career', 'fhtc' ),
	'view_item'          => __( 'View Career', 'fhtc' ),
	'search_items'       => __( 'Search Careers', 'fhtc' ),
	'not_found'          => __( 'No Careers found', 'fhtc' ),
	'not_found_in_trash' => __( 'No Careers found in Trash', 'fhtc' ),
	'parent_item_colon'  => __( 'Parent Career:', 'fhtc' ),
	'menu_name'          => __( 'Careers', 'fhtc' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'show_in_rest'		  => true,
	'menu_icon'           => 'dashicons-businessman',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true, // main career listing/group page
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array( 'title' ),
);
register_post_type( 'careers', $args );