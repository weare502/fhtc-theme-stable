<?php

$labels = array(
	'name'               => __( 'Programs', 'fhtc' ),
	'singular_name'      => __( 'Program', 'fhtc' ),
	'add_new'            => _x( 'Add Program', 'fhtc', 'fhtc' ),
	'add_new_item'       => __( 'Add Program', 'fhtc' ),
	'edit_item'          => __( 'Edit Program', 'fhtc' ),
	'new_item'           => __( 'New Program', 'fhtc' ),
	'view_item'          => __( 'View Program', 'fhtc' ),
	'search_items'       => __( 'Search Programs', 'fhtc' ),
	'not_found'          => __( 'No Programs found', 'fhtc' ),
	'not_found_in_trash' => __( 'No Programs found in Trash', 'fhtc' ),
	'parent_item_colon'  => __( 'Parent Program:', 'fhtc' ),
	'menu_name'          => __( 'Programs', 'fhtc' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array('program-type'),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'show_in_rest'		  => true,
	'menu_icon'           => 'dashicons-welcome-learn-more',
	'show_in_nav_menus'   => true,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => false, // use   all-programs.php/twig/scss
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array( 'title', 'thumbnail', 'excerpt' ),
);
register_post_type( 'program', $args );