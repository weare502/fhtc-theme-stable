<?php

$labels = array(
	'name'               => __( 'Employees', 'fhtc' ),
	'singular_name'      => __( 'Employee', 'fhtc' ),
	'add_new'            => _x( 'Add Employee', 'fhtc', 'fhtc' ),
	'add_new_item'       => __( 'Add Employee', 'fhtc' ),
	'edit_item'          => __( 'Edit Employee', 'fhtc' ),
	'new_item'           => __( 'New Employee', 'fhtc' ),
	'view_item'          => __( 'View Employee', 'fhtc' ),
	'search_items'       => __( 'Search Employees', 'fhtc' ),
	'not_found'          => __( 'No Employees found', 'fhtc' ),
	'not_found_in_trash' => __( 'No Employees found in Trash', 'fhtc' ),
	'parent_item_colon'  => __( 'Parent Employee:', 'fhtc' ),
	'menu_name'          => __( 'Employees', 'fhtc' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'show_in_rest'		  => true,
	'menu_icon'           => 'dashicons-nametag',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => false,
	'exclude_from_search' => true,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array( 'title' ),
);
register_post_type( 'employee', $args );