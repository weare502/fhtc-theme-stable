<?php
/**
 * Default Template.
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$templates = array( 'page.twig' );

// add child menu

// Keep - client may want in future

// $parent_id = $post->post_parent;
// $children = array();

// if( $parent_id !== 0 ) {
// 	$children = Timber::get_posts([
// 		'post_parent' => $parent_id,
// 		'posts_per_page' => 25,
// 		'post_type' => 'page',
// 		'order' => 'ASC'
// 	]);
// }
// $context['children'] = $children;

Timber::render( $templates, $context );