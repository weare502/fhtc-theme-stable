<?php
/**
 * Template Name: Default - No Hero
 * Default Page with no hero image.
 * 
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$templates = array( 'no-hero-page.twig' );

Timber::render( $templates, $context );