<?php
/**
 * Template Name: Admissions
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$templates = array( 'admissions.twig' );
Timber::render( $templates, $context );