<?php
/**
 * Template Name: All Programs
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

// default ordering is   'orderby' => 'menu_order'    which is alphabetical
$context['program_types'] = Timber::get_terms([ 'taxonomies' => 'program-type' ]);

$context['programs'] = Timber::get_posts([
	'post_type' => 'program',
	'posts_per_page' => -1,
	'orderby' => 'title',
	'order' => 'ASC'
]);

// filter / search feature
$templates = array( 'all-programs.twig' );
Timber::render( $templates, $context );