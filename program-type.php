<?php
/**
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
*/
// from router
global $params;

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$program_data = fhtc_get_program_from_advising_data( $post->get_field('program_prefix') );

$program_type_data = fhtc_get_program_type_data( $program_data, $params['type'] );

// echo '<pre>';
// var_dump($program_type_data); die();

$context['program_type_data'] = $program_type_data;
Timber::render( array( 'program-type.twig' ), $context );