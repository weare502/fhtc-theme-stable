<?php

// load the dependencies for composer and the api-client packages
require __DIR__ . '/vendor/autoload.php';

// setup the Client and Calendar ID / Request Endpoint
// some objects are writable so it must be 'Read Only' to protect calendar data
$client = new Google_Client();
$client->setAuthConfig( __DIR__ . '/fhtc-c145-f736b7238c9c.json' );
$client->setScopes( 'https://www.googleapis.com/auth/calendar.events.readonly' );

// setup calendar service and data (Uses a Google Service Worker)
$calendarService = new Google_Service_Calendar($client);
$calendarID = "fhtc.edu_j5q4qtbjb6ljd44pok4ieojd8o@group.calendar.google.com";

// get our cached data
$events = get_transient( 'cal_transient' );

// no cached data found / update request
if( empty($events) ) {
	// Use the Calendar Service Worker to poll the Calendar API for Events
	$events = $calendarService->events->listEvents(
		$calendarID, array(
			'maxResults' => 70,
			'orderBy' => 'startTime',
			'singleEvents' => true,
			'showDeleted' => false,
			'timeMin' => date(DATE_RFC3339), // DOES NOT ACCEPT STRING DATE VALUE
		)
	)->getItems();

	// cache for 3 hours before polling again
	set_transient( 'cal_transient', $events, 3 * HOUR_IN_SECONDS );
}

// simple check to see if a Transient exists or not
// if( false === ( $events = get_transient( 'remote_posts' ) ) ) {
// 	 echo '<p>There is no Transient set for this Object!</p>';
// 	 echo '<pre>' . var_dump($events) . '</pre>';
// }

$count = 1; /** Can not be in the loop
each 'if' check needs this to start at 1 and increment only within the calling 'if' block */

// make sure events is not empty
if( ! empty($events) ) {
	foreach( $events as $event ) :

		// account for daylights savings time - Days need to be updated each year
		// dst = daylights savings time
		// Link for DST: https://www.timeanddate.com/time/change/usa
		$march_dst_2020 = '2020-03-08';
		$november_dst_2020 = '2020-11-01';
		$march_dst_2021 = '2021-03-14';
		$start_date = date($event->start->dateTime); // initial start date (DO NOT FORMAT THIS)

		if( $start_date >= $march_dst_2020 && $start_date < $november_dst_2020 ) {
			// move clock forward 1 hour
			$start_date = date('F j', strtotime($event->start->dateTime) - 60 * 60 * 5);
			$end_date = date('F j', strtotime($event->end->dateTime) - 60 * 60 * 5);
			$start_day = date('l', strtotime($event->start->dateTime) - 60 * 60 * 5);
			$end_day = date('l', strtotime($event->end->dateTime) - 60 * 60 * 5);

			// TimeZone returned is 5 hours ahead (UTC+5:00) we need (GMT-5:00)
			$start_time = date('g:ia', strtotime($event->start->dateTime) - 60 * 60 * 5);
			$end_time = date('g:ia', strtotime($event->end->dateTime) - 60 * 60 * 5);

		} else if( $start_date >= $november_dst_2020 && $start_date < $march_dst_2021 ) {
			// Move clock backward 1 hour
			$start_date = date('F j', strtotime($event->start->dateTime) - 60 * 60 * 6);
			$end_date = date('F j', strtotime($event->end->dateTime) - 60 * 60 * 6);
			$start_day = date('l', strtotime($event->start->dateTime) - 60 * 60 * 6);
			$end_day = date('l', strtotime($event->end->dateTime) - 60 * 60 * 6);

			// TimeZone returned is 6 hours ahead (UTC+6:00) we need (GMT-6:00)
			$start_time = date('g:ia', strtotime($event->start->dateTime) - 60 * 60 * 6);
			$end_time = date('g:ia', strtotime($event->end->dateTime) - 60 * 60 * 6);
		}

		// All Day Events use date() and not dateTime()
		$all_day_event = date('F j, Y', strtotime($event->start->date));

		// get event specific details (description is not displayed but is used for sorting)
		$location = $event->location;
		$description = $event->description;
		$title = $event->getSummary();

		// find locations that have a comma somewhere in the string and break off at the comma
		if( ! empty($location) && strpos($location, ',') ) :
			$loc_address = stristr($location, ',', true);

		// catch the other strings without a comma (first check will grab the comma ones)
		elseif( ! empty($location) ) :
			$loc_address = $location;

		// if it is empty, display a message instead of nothing
		else :
			$loc_address = 'No Location';
		endif;

		// End of vars and data setup //
		?>

		<?php
		// Home Page
		if( is_front_page() && strpos($description, '#featured') !== false ) :
			if( $count < 4) : ?>
			<div class="calendar-grid__event">
				<time>
					<?php if( $all_day_event != 'January 1, 1970') : ?>
						<?= date('l', strtotime($all_day_event)); ?>
						<span class="middot">&middot;</span>
						<?= date('F j', strtotime($all_day_event)); ?>
					<?php else : ?>
						<?= $start_day; ?>
						<span class="middot">&middot;</span>
						<?= $start_date; ?>
					<?php endif; ?>
				</time>

				<h5 class="h5-heading--w"><?= $title; ?></h5>

				<div class="push-down">
					<span class="fa fa-map-marker"></span>
					<span class="grid-location"><?= $loc_address; ?></span>
				</div>

				<div>
					<span class="fa fa-clock-o"></span>
					<?php if( $all_day_event != 'January 1, 1970') : ?>
						<span class="grid-time">All Day</span>
					<?php else : ?>
						<span class="grid-time"><?= $start_time; ?></span>
					<?php endif; ?>
				</div>
			</div>
			<?php $count += 1; endif; ?>

		<?php // Admissions Page or Enrollment Page ?>
		<?php elseif( (is_page(20) && strpos($description, '#enrollment') !== false) || (is_page(94) && strpos($description, '#enrollment') !== false) ) :
			if( $count < 4 ) : ?>
			<div class="calendar-grid__event">
				<time>
					<?php if( $all_day_event != 'January 1, 1970') : ?>
						<?= date('l', strtotime($all_day_event)); ?>
						<span class="middot">&middot;</span>
						<?= date('F j', strtotime($all_day_event)); ?>
					<?php else : ?>
						<?= $start_day; ?>
						<span class="middot">&middot;</span>
						<?= $start_date; ?>
					<?php endif; ?>
				</time>

				<h5 class="h5-heading--w"><?= $title; ?></h5>

				<div class="push-down">
					<span class="fa fa-map-marker"></span>
					<span class="grid-location"><?= $loc_address; ?></span>
				</div>

				<div>
					<span class="fa fa-clock-o"></span>
					<?php if( $all_day_event != 'January 1, 1970') : ?>
						<span class="grid-time">All Day</span>
					<?php else : ?>
						<span class="grid-time"><?= $start_time; ?></span>
					<?php endif; ?>
				</div>
			</div>
			<?php $count += 1; endif; ?>

		<?php // Student Life Page ?>
		<?php elseif( is_page(4036) && strpos($description, '#studentlife') !== false ) :
			if( $count < 4 ) : ?>
				<div class="calendar-grid__event">
					<time>
						<?php if( $all_day_event != 'January 1, 1970') : ?>
							<?= date('l', strtotime($all_day_event)); ?>
							<span class="middot">&middot;</span>
							<?= date('F j', strtotime($all_day_event)); ?>
						<?php else : ?>
							<?= $start_day; ?>
							<span class="middot">&middot;</span>
							<?= $start_date; ?>
						<?php endif; ?>
					</time>

					<h5 class="h5-heading--w"><?= $title; ?></h5>

					<div class="push-down">
						<span class="fa fa-map-marker"></span>
						<span class="grid-location"><?= $loc_address; ?></span>
					</div>

					<div>
						<span class="fa fa-clock-o"></span>
						<?php if( $all_day_event != 'January 1, 1970') : ?>
							<span class="grid-time">All Day</span>
						<?php else : ?>
							<span class="grid-time"><?= $start_time; ?></span>
						<?php endif; ?>
					</div>
				</div>
			<?php $count += 1; endif; ?>

		<?php // Community Outreach Page ?>
		<?php elseif( is_page(156) && strpos($description, '#community') !== false ) :
			if( $count < 4 ) : ?>
				<div class="calendar-grid__event">
					<time>
						<?php if( $all_day_event != 'January 1, 1970') : ?>
							<?= date('l', strtotime($all_day_event)); ?>
							<span class="middot">&middot;</span>
							<?= date('F j', strtotime($all_day_event)); ?>
						<?php else : ?>
							<?= $start_day; ?>
							<span class="middot">&middot;</span>
							<?= $start_date; ?>
						<?php endif; ?>
					</time>

					<h5 class="h5-heading--w"><?= $title; ?></h5>

					<div class="push-down">
						<span class="fa fa-map-marker"></span>
						<span class="grid-location"><?= $loc_address; ?></span>
					</div>

					<div>
						<span class="fa fa-clock-o"></span>
						<?php if( $all_day_event != 'January 1, 1970') : ?>
							<span class="grid-time">All Day</span>
						<?php else : ?>
							<span class="grid-time"><?= $start_time; ?></span>
						<?php endif; ?>
					</div>
				</div>
			<?php $count += 1; endif; ?>

		<?php // All Events ?>
		<?php elseif( is_page(3981) ) : ?>
			<div class="event-list">
				<div class="event-list--left">

					<?php
					// initial check against date parameter used (all day events use different param)
					if( $all_day_event != 'January 1, 1970' ) :
						// we check WITH the year above, then strip the year 
						$all_day_date = date('F j', strtotime($event->start->date));
					?>
						<span class="h5-heading"><?= $all_day_date; ?></span>
					<?php elseif($start_date == $end_date) : ?>
						<span class="h5-heading"><?= $start_date; ?></span>
					<?php else : ?>
						<span class="h5-heading"><?= $start_date . ' - ' . $end_date; ?></span>
					<?php endif; ?>
					<h5 class="mobile-event-title" style="margin: 0;"><?= $title; ?></h5>

					<div class="e-location">
						<span class="fa fa-map-marker"></span>
						<p><?= $loc_address; ?></p>
					</div>

					<div class="e-time">
						<span class="fa fa-clock-o"></span>
						<?php if( $all_day_event != 'January 1, 1970' ) : ?>
							<p>All Day</p>
						<?php elseif($start_time == $end_time) : ?>
							<p><?= $start_time; ?></p>
						<?php else : ?>
							<p><?= $start_time . ' - ' . $end_time; ?></p>
						<?php endif; ?>
					</div>
				</div>

				<div class="event-list--right">
					<h5 style="margin: 0;"><?= $title; ?></h5>
				</div>
			</div>
		<?php endif;
	endforeach;
} // end of !empty($events)