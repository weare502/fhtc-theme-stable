<?php
/**
 * Template Name: Foundation Home
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$templates = array( 'foundation/foundation-home.twig' );

Timber::render( $templates, $context );