<?php
/**
 * Template Name: Course Schedules
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$imports = Timber::get_posts( [
	'post_type' => 'course',
	'posts_per_page' => -1,
	'orderby' => 'title',
	'order' => 'ASC'
] );

$context['imports'] = $imports;

$templates = array( 'course-schedules.twig' );

Timber::render( $templates, $context );