<?php
/**
 * Displays the Search Results
 * 
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();

// Access the Post Query Object so we can loop through results
$context['posts'] = new Timber\PostQuery();
$context['pagination'] = Timber::get_pagination();

global $wp_query;
$big = 999999999; // need an unlikely integer
 
$context['paginate'] = paginate_links( array(
    'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
    'format'  => '?paged=%#%',
    'current' => max( 1, get_query_var('paged') ),
	'total'   => $wp_query->max_num_pages,
	'prev_next' => false,
	'show_all' => true,
) );

$context['results_count'] = $wp_query->found_posts;
$context['get_query_string'] = get_search_query();
$context['cleanup'] = wp_reset_postdata();

$templates = array( 'search-results.twig' );

Timber::render( $templates, $context );