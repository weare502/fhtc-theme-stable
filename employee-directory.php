<?php
/**
 * Template Name: Employee Directory
*/

$context = Timber::get_context();

$employees = Timber::get_posts([
    'post_type' => 'employee',
	'posts_per_page' => -1,
	'meta_key' => 'emp_last_name',
    'orderby' => 'meta_value',
    'order' => 'ASC'
]);

$alphabet = []; // Get and Set letters
$departments = []; // controls the filter dropdown

foreach ( $employees as $em ){
    $letter = substr($em->get_field('emp_last_name'), 0, 1);
    if ( isset($alphabet[$letter]) ){
        $alphabet[$letter][] = $em;
    } else {
        $alphabet[$letter] = [$em];
    }

    $department = $em->get_field('employee_filter_name');

    if ( ! in_array($department, $departments) ){
        $departments[] = $department;
    }
}

// alphabetical order of filter names
sort($departments);

$context['employees'] = $alphabet;

//used to populate search employees 
$context['all_employees'] = $employees;
$context['departments'] = $departments;
$context['post'] = Timber::get_post();

Timber::render( 'employee-directory.twig', $context );